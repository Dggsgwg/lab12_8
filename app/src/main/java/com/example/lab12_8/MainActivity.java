package com.example.lab12_8;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText f, s, t;
    TextView res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        f = findViewById(R.id.first);
        s = findViewById(R.id.second);
        t = findViewById(R.id.third);

        res = findViewById(R.id.result);
    }

    public void onCalculate(View v) {
        double fv = 0;
        double sv = 0;
        double tv = 0;
        try {
            fv = Double.parseDouble(f.getText().toString());
            sv = Double.parseDouble(s.getText().toString());
            tv = Double.parseDouble(t.getText().toString());
        } catch (Exception e) {}

        res.setText(String.format("%.2f", (fv+sv+tv) / 3));
    }

    public void onClear(View v) {
        f.setText("");
        s.setText("");
        t.setText("");
        res.setText("");
    }
}